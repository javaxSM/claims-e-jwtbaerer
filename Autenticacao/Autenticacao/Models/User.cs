﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Autenticacao.Models
{
    public class User
    {
        public int Id { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }
        public string Role { get; set; }
    }
}
