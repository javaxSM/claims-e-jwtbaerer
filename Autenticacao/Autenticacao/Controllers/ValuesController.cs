﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autenticacao.Models;
using Autenticacao.Repository;
using Autenticacao.Services;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Autenticacao.Controllers
{
    [Route("v1/conta")]
    [ApiController]
    public class usuarioController : ControllerBase
    {
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Login([FromBody] User model)
        {
            var user = UserRepository.Get(model.usuario, model.senha);

            if (user == null)
                return NotFound(new { message = "Usuário ou senha inválidos" });

            var token = TokenServices.CriarToken(user);

            user.senha = "";

            return new
            {
                user = user,
                token = token
            };
        }

        [HttpGet]
        [Route("Anonymous")]
        [AllowAnonymous]
        public string Anonymous() => "Anõnimo";

        [HttpGet]
        [Route("autorizado")]
        [Authorize]
        public string Autorizado() => string.Format("Autorizado - {0} ", User.Identity.Name);

        [HttpGet]
        [Route("acesso/adm")]
        [Authorize(Roles = "administrador")]
        public string ApenasAdm() => string.Format("Autorizado - {0}", User.Identity.Name);

        [HttpGet]
        [Route("acesso/funcionario")]
        [Authorize(Roles = "administrador, funcionario")]
        public string Todo() => string.Format("Autorizado - {0}", User.Identity.Name);
    }
}
