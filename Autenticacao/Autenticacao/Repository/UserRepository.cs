﻿using Autenticacao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Autenticacao.Repository
{
    public class UserRepository
    {
        public static User Get(string usuario, string senha)
        {
            var users = new List<User>();

            users.Add(new User { Id = 1, usuario = "admin", senha = "admin", Role = "administrador" });
            users.Add(new User { Id = 2, usuario = "funcionario", senha = "funcionario", Role = "funcionarios" });

            return users.Where(x => x.usuario.ToLower() == usuario.ToLower() &&
                                x.senha == senha).First();
        }
    }
}
